# Generated by Django 4.0.3 on 2023-01-24 18:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0002_remove_appointment_automobile'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='date',
            field=models.DateTimeField(null=True),
        ),
    ]
