import React from 'react'

function SalesList(props) {
  return (
    <>
      <h1>Sales Record</h1>
        <table className="table table-striped">
        <thead>
            <tr>
                <th>Sales Person</th>
                <th>Employee Number</th>
                <th>Customer</th>
                <th>VIN</th>
                <th>Sale Price</th>
            </tr>
        </thead>
        <tbody>
            {props.sales.map(sale => {
            return (
                <tr key={sale.id}>
                    <td>{sale.sales_person.name}</td>
                    <td>{sale.sales_person.emp_no}</td>
                    <td>{sale.customer}</td>
                    <td>{sale.automobile}</td>
                    <td>{sale.price}</td>
                </tr>
                );
            })}
        </tbody>
    </table>
  </>
  )
}

export default SalesList
