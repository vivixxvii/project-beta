import React, {useEffect, useState} from 'react'

function SalesHistoryList(props) {
    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    return (
    <>
        <h1>Sales Person History</h1>
        <div className="mb-3">
            <select value={name} onChange={handleNameChange} required id="name" name="name" className="form-select">
                <option value="">Choose a Sales Person</option>
                {props.salesperson.map(salesp => {
                        return (
                            <option key={salesp.id} value={salesp.name}>
                                {salesp.name}
                            </option>
                        );
                    })}
            </select>
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Sales Person</th>
                    <th>Employee Number</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {props.sales.filter(sale => sale.sales_person.name === name)
                .map(sale => {
                return (
                    <tr key={sale.automobile}>
                        <td>{sale.sales_person.name}</td>
                        <td>{sale.sales_person.emp_no}</td>
                        <td>{sale.customer}</td>
                        <td>{sale.automobile}</td>
                        <td>${sale.price}</td>
                    </tr>
                    );
                })}
            </tbody>
        </table>

    </>
      )
}

export default SalesHistoryList
