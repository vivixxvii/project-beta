import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import { useEffect, useState } from 'react'
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import SalesPersonForm from './SalespersonForm'
import CustomerForm from './CustomerForm';
import SalesForm from './SalesForm';
import SalesList from './SalesList';
import SalesHistoryList from'./SalesHistoryList';
import AppointmentList from './AppointmentList';
import AppointmentForm from './AppointmentForm';
import AutomobileForm from './AutomobileForm';
import AutomobileList from  './AutomobileList';
import ServiceHistory from './ServiceHistory';
import TechnicianForm from './TechnicianForm';
import VehicleModelForm from './VehicleModelForm';
import VehicleModelList from './VehicleModelList';

function App() {
  const [manufacturers, setManufacturers] = useState([]);
  const fetchManufacturers = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers)
    }
  }

  const [models, setModels] = useState([]);
  const fetchModels = async () => {
    const url = 'http://localhost:8100/api/models/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models)
      }
  }

  const [salesperson, setSalesPersons] = useState([]);
  const fetchSalesPerson = async () => {
    const url = "http://localhost:8090/api/salesperson/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalesPersons(data.salespersons)
      }

  }

  const [customer, setCustomer] = useState([]);
  const fetchCustomer = async () => {
    const url = "http://localhost:8090/api/customers/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setCustomer(data.customers)
      }
  }

  const [sales, setSales] = useState([]);
  const fetchSales = async () => {
    const url = "http://localhost:8090/api/salesrecords/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSales(data.salesrecords)
    }
  }



  const [automobile, setAutomobile] = useState([]);
  const fetchAutomobile = async () => {
    const url = "http://localhost:8090/api/automobiles/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAutomobile(data)

    }
  }
  const [automobiles, setAutomobiles] = useState([]);
  const fetchAutomobiles = async () => {
    const url = "http://localhost:8100/api/automobiles/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos)

    }
  }

  const [technicians, setTechnician] = useState([]);
  const fetchTechnician = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setTechnician(data)
      }
  }
  const [appointments, setAppointments] = useState([]);
  const fetchAppointments = async () => {
    const url = 'http://localhost:8080/api/appointments/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments)
      }
  }



  useEffect(() => {
    fetchManufacturers();
    fetchModels();
    fetchCustomer();
    fetchSales();
    fetchAutomobile();
    fetchSalesPerson();
    fetchTechnician();
    fetchAppointments();
    fetchAutomobiles();
  },[]);


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="manufacturers" >
            <Route path="" element={<ManufacturerList manufacturers={manufacturers} fetchManufacturers={fetchManufacturers}/>} />
            <Route path="new" element={<ManufacturerForm fetchManufacturers={fetchManufacturers} />}  />
          </Route>
          <Route path="models/">
            <Route path="" element={<VehicleModelList models={models} />} />
            <Route path="new" element={<VehicleModelForm fetchModels={fetchModels} />} />
          </Route>
          <Route path="salesperson/">
            <Route path="new" element={<SalesPersonForm salesperson={salesperson} fetchSalesPerson={fetchSalesPerson} />}/>
          </Route>
          <Route path="automobiles/">
            <Route path="" element={<AutomobileList automobiles={automobiles} fetchAutomobiles={fetchAutomobiles}/>} />
            <Route path="new" element={<AutomobileForm models={models} automobiles={automobiles} fetchModels={fetchModels} fetchAutomobiles={fetchAutomobiles}/>} />
          </Route>
          <Route path="customer/">
            <Route path="new" element={<CustomerForm customer={customer} fetchCustomer={fetchCustomer}/>}/>
          </Route>
          <Route path="sales/">
            <Route path="new" element={<SalesForm customer={customer} automobile={automobile} salesperson={salesperson} fetchAutomobile={fetchAutomobile} fetchSalesPerson={fetchSalesPerson} fetchCustomer={fetchCustomer} fetchSales={fetchSales}/>} />
            <Route path="" element={<SalesList sales={sales}/>}/>
            <Route path="history" element={<SalesHistoryList salesperson={salesperson} sales={sales} fetchSales={fetchSales} fetchSalesPerson={fetchSalesPerson}/>}/>
          </Route>
          <Route path="technicians/">
            <Route path="new" element={<TechnicianForm technicians={technicians} fetchTechnician={fetchTechnician}/>} />
          </Route>
          <Route path="appointments/">
            <Route path="new" element={<AppointmentForm customer={customer} technicians={technicians} appointments={appointments} fetchAppointments={fetchAppointments} fetchTechnician={fetchTechnician} fetchCustomer={fetchCustomer}/>} />
            <Route path="" element={<AppointmentList appointments={appointments} fetchAppointments={fetchAppointments}/>}/>
            <Route path="history" element={<ServiceHistory appointments={appointments} fetchAppointments={fetchAppointments}/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
