import React, {useEffect, useState} from 'react'

function SalesPersonForm(props) {
    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const [employeeNumber, setEmployeeNumber] = useState('');
    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value;
        setEmployeeNumber(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.employee_number = employeeNumber

        const salesPersonUrl = "http://localhost:8090/api/salesperson/";

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(salesPersonUrl, fetchConfig);
        if (response.ok) {
            const newSalesPerson = await response.json();
            console.log(newSalesPerson)
            setName("");
            setEmployeeNumber("");
            props.fetchSalesPerson()


        }
    }
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Salesperson!</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Fabric" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={employeeNumber} onChange={handleEmployeeNumberChange} placeholder="Fabric" required type="text" name="employee_number" id="employee_number" className="form-control"/>
                <label htmlFor="employee_number">Employee Number</label>
              </div>
              <button variant="contained" size="medium" style={{backgroundColor:"black",
            fontWeight:"normal", color:"white" }} className="btn btn">Create</button>
            </form>
          </div>
        </div>
      </div>

    )
}

export default SalesPersonForm
