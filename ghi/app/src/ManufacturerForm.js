import React, {useEffect, useState} from 'react'

function ManufacturerForm(props) {
    const [name, setName] = useState('');
    const [submitted, setSubmitted] = useState(false);
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;

        const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
            const newManufacturer = await response.json();
            setName('');
            setSubmitted(true);
            props.fetchManufacturers()


        }
    }
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a manufacturer!</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Fabric" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <button variant="contained" size="medium" style={{backgroundColor:"black",
            fontWeight:"normal", color:"white" }} className="btn btn">Create</button>
            </form>
            <p></p>
                    {submitted && (
                        <div className="alert alert-success mb-0" id="success-message">
                            Your manufacturer has been successfully created. Thank you!
                        </div>
                    )}
          </div>
        </div>
      </div>

    )
}

export default ManufacturerForm
