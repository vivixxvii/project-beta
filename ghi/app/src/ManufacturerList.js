import React, {useEffect, useState} from 'react'

function ManufacturerList(props) {

//Our JSX that renders our table
    return (
    <>
        <h1>Manufacturers</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                {props.manufacturers.map(manufacturer => {
                return (
                    <tr key={manufacturer.name}>
                        <td  >{ manufacturer.name }</td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    );
  }

export default ManufacturerList;
