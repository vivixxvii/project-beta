import React, {useState} from "react";


function AppointmentList({appointments, fetchAppointments}){
    const [finished, setFinished] = useState(false);
    const [cancelled, setCancelled] = useState(false);

    if (appointments === undefined) {
        return null;
    }

    

    const cancelAppointment = async (appointment) => {
        const appointmentUrl = `http://localhost:8080/api/appointments/${appointment.id}/`;
        
        const fetchConfig = {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(appointmentUrl, fetchConfig);
    
        if (response.ok) {
            fetchAppointments();
            setCancelled(true);
            setFinished(false);
    
        }
    }
    const finishedAppointment = async (appointment) => {
        const appointmentUrl = `http://localhost:8080/api/appointments/${appointment.id}/`
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({ finished: true }),
            headers: {
                "Content-Type": "application/json",
            },
        };
    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
        fetchAppointments();
        setFinished(true);
        setCancelled(false);

		}
	}

return (
        <>
        <div className='p-5 text-center'>
            <h1 className='mb-3'>Service Appointments</h1>
        </div>
        {cancelled && (
            <div className='alert alert-success mb-0' id="success-message">
                The appointment has been cancelled. Thank You!
            </div>
        )}
        {finished && (
            <div className='alert alert-success mb-0' id="success-message">
                The appointment is completed. Thank You!
            </div>
        )}

        <table className="table table-striped">
        <thead>
            <tr>
            <th>Customer Name</th>
            <th>Vin</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>VIP</th>
            <th>Finished</th>
            </tr>
        </thead>
        <tbody>
            {appointments.map(appointment => {
            if(!appointment.completed) {
                return (
                <tr key={appointment.id}>
                    <td>{ appointment.customer_name }</td>
                    <td>{ appointment.vin }</td>
                    <td>{new Date(appointment.appointment_date_time).toLocaleDateString("en-US")}</td>
                    <td>{new Date(appointment.appointment_date_time).toLocaleTimeString([], {
                                        hour: "2-digit",
                                        minute: "2-digit",
                                    })}</td>
                    <td>{ appointment.technician}</td>
                    <td>{ appointment.reason }</td>
                    <td>{ appointment.vip ? "Yes" : "No"}</td>
                    <td>{ appointment.finished ? "Yes" : "No"}</td>
                    <td>
                        <button onClick={() => cancelAppointment(appointment)}
                        type="button" className="btn btn-outline-danger">Cancel</button>
                    </td>
                    <td>
                        <button onClick={() => finishedAppointment(appointment)}
                        type="button" className="btn btn-outline-success">Finished</button>
                    </td>
                </tr>
                );
            }
            })}
            </tbody>
        </table>
    </>
    );
}
export default AppointmentList;

